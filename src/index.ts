type Config = {
    /** Дата рождения */
    birthday: Date,
    /** Начало инвестирования */
    planStartInvesting: Date,
    /** Планавая ежемесячная зарплата */
    planMonthMoney: number,
    /** Пдановая средняя инфляция */
    planInflation: number,
    /** Плановый доход при активном инвестировании о пенсии */
    planInvestingIncome: number,
    /** Плановый доход на пенсии */
    planDividentsIncome: number,
    /** Средняя ставка налога на доход от инвестиций */
    avgTax: number,
    /** Ежегодное инвестирование */
    investmen: number,
    /** Максимальный ожидаемый возраст */
    maxAge: number,
    /**
     * Деньги, которые могут быть получены в качестве
     * наследства или накоплены заранее и не учавствующие
     * в инвестировании, но могущие повлиять на результат
     */
    startMoney: number,
};

const defaultConfig: Config = {
    birthday: new Date(1987, 2, 25),
    planStartInvesting: new Date(2021, 1, 1),
    planMonthMoney: 50000,
    planInflation: 0.03,
    planInvestingIncome: 0.50,
    planDividentsIncome: 0.10,
    avgTax: 0.13,
    investmen: 50000,
    maxAge: 100,
    startMoney: 0,
};

main();

function main(config = defaultConfig) {
    const needYears = countYears(config);

    // console.log(getMoneyAccum(config, 2));

    if (needYears === undefined) {
        console.log('Невозможно');
        return;
    }

    console.log(`Нужно ${needYears} лет чтобы накопить ${getMoneyAccum(config, needYears)} руб.`);
    console.log(config.planMonthMoney * 12 * Math.pow(1 + config.planInflation, needYears));
}

function countYears(config = defaultConfig) {
    const endYear = config.birthday.getFullYear() + config.maxAge;
    const years = endYear - config.planStartInvesting.getFullYear();

    let yearsNeeded = 0;

    for (let i = 1; i <= years; i++) {
        const gettedMoney = getMoneyAccum(config, i) + config.startMoney;
        const planMonthMoney = config.planMonthMoney * Math.pow(1 + config.planInflation, i);
        console.log(planMonthMoney);
        const needMoney = getManyNeed({...config, planMonthMoney}, years - i);

        if (gettedMoney > needMoney) {
            return i;
        }
    }
}

function getMoneyAccum(conf = defaultConfig, N = 60) {
    const ks = sumKoeff(conf.planInvestingIncome, conf.avgTax);
    function fi(i: number) {
        return Math.pow(ks, i);
    }
    return conf.investmen * sum(0, N - 1, fi);
}

function getManyNeed(conf = defaultConfig, N = 60) {
    const kz = salaryKoeff(conf.planInflation);
    const ks = sumKoeff(conf.planDividentsIncome, conf.avgTax);
    function fi(i: number) {
        return Math.pow(ks, i) * Math.pow(kz, N - i);
    }
    const z = conf.planMonthMoney * 12
    const summa = sum(1, N - 1, fi);
    return z * (1 + summa / Math.pow(ks, N));
}

function sumKoeff(incomePercent: number, taxPercent: number) {
    return 1 + incomePercent - incomePercent * taxPercent;
}

function salaryKoeff(inflationPercent: number) {
    return 1 + inflationPercent;
}

function sum(start: number, N: number, fi: (i: number) => number) {
    let sum = 0;

    for (let i = start; i <= N; i++) {
        sum += fi(i);
    }

    return sum;
}
